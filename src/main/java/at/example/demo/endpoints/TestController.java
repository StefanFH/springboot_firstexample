package at.example.demo.endpoints;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController("/test")
public class TestController {

    @RequestMapping(method = RequestMethod.GET)
    public String getAllItems() {
        return "Hello from Spring Boot and Heroku!";
    }
}
